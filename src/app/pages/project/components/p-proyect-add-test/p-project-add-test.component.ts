import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'p-project-add-test',
  templateUrl: 'p-project-add-test.component.html',
  styleUrls: ['p-project-add-test.component.scss']
})
export class PProjectAddTestComponent implements OnInit {

  @Input() proyectId: string;
  isVersionRoast: boolean = false;
  @Output () onClose: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  close(){
    this.onClose.emit(true)
  }
}
