import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PProjectAddTestComponent } from './p-project-add-test.component';

describe('PProjectAddTestComponent', () => {
  let component: PProjectAddTestComponent;
  let fixture: ComponentFixture<PProjectAddTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PProjectAddTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PProjectAddTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
