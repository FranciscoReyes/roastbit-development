import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'rbit-modal-change-version',
  templateUrl: './modal-change-version.component.html',
  styleUrls: ['./modal-change-version.component.scss']
})
export class ModalChangeVersionComponent implements OnInit {

  @Output() cancel : EventEmitter<any> = new EventEmitter<any>();
  @Output() accept : EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  acceptClicked() {
    this.accept.emit(true);
  }

  cancelClicked() {
    this.cancel.emit(true)
  }

}
