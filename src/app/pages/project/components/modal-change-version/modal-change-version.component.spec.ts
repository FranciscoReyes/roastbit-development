import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalChangeVersionComponent } from './modal-change-version.component';

describe('ModalChangeVersionComponent', () => {
  let component: ModalChangeVersionComponent;
  let fixture: ComponentFixture<ModalChangeVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalChangeVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalChangeVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
