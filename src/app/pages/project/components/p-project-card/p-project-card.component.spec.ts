import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PProjectCardComponent } from './p-project-card.component';

describe('PProjectCardComponent', () => {
  let component: PProjectCardComponent;
  let fixture: ComponentFixture<PProjectCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PProjectCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PProjectCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
