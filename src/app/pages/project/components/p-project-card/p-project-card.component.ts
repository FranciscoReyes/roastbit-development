import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'p-project-card',
  templateUrl: 'p-project-card.component.html',
  styleUrls: ['p-project-card.component.scss']
})
export class PProjectCardComponent implements OnInit {

  @Input() projectType: string;
  @Input() projectName: string;
  @Input() projectDate: string;
  @Input() projectDescription: string;
  @Input() projectTags: Array<string> = [];
  @Input() projectTestingCount: number = 0;
  @Input() start: boolean = false;
  @Input() type: string ;
  @Input() avatar: any = [
    { img: './../../../../../assets/img/proyects/Layer2.png' },
    { img: './../../../../../assets/img/proyects/Layer2.png' },
  ];

  constructor() { }

  ngOnInit() {
  }

}
