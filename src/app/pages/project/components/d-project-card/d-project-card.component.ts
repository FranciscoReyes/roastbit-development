import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'd-project-cards',
  templateUrl: 'd-project-card.component.html',
  styleUrls: ['d-project-card.component.scss']
})
export class DProjectCardsComponent implements OnInit, OnChanges {

  @Input() projectName: string;
  @Input() projectVersion: string;
  @Input() projectGoal: string;
  @Input() projectTestCount: number = 0;
  @Input() projectTestTotalCount: number = 1;
  @Input() projectSuccessPercentage: number = 0;
  @Input() projectErrorPercentage: number = 0;
  @Input() projectInfoPercentage: number = 0;
  @Input() projectWarningPercentage: number = 0;
  @Input() projectAvatar: any = [];
  displayProgressBarAvatar: boolean = true;
  displayProgressBar: boolean = true;
  projectPercentage:string  ='';
  
  constructor() {
    this.displayProgressBar = (
      this.projectSuccessPercentage + this.projectErrorPercentage
      + this.projectInfoPercentage + this.projectWarningPercentage
    ) <= 100;
  }

  ngOnInit() {
    this.projectPercentage = ((this.projectTestCount / this.projectTestTotalCount) * 100) + "% ";
  }

  ngOnChanges(changes: SimpleChanges) {
    this.toggleProgressBar(changes);
  }

  toggleProgressBar(changes: SimpleChanges) {
    const success = !!changes['projectSuccessPercentage']
      ? changes['projectSuccessPercentage'].currentValue : 0;
    const error = !!changes['projectErrorPercentage']
      ? changes['projectErrorPercentage'].currentValue : 0;
    const info = !!changes['projectInfoPercentage']
      ? changes['projectInfoPercentage'].currentValue : 0;
    const warning = !!changes['projectWarningPercentage']
      ? changes['projectWarningPercentage'].currentValue : 0;
    this.displayProgressBar = (success + error + info + warning) <= 100;
  }
}
