import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DProjectCardComponent } from './d-project-card.component';

describe('DProjectCardComponent', () => {
  let component: DProjectCardComponent;
  let fixture: ComponentFixture<DProjectCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DProjectCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DProjectCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
