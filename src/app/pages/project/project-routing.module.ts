import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {ProjectComponent} from "./project.component";

const routes: Routes = [
  {
    path: "",
    component: ProjectComponent,
    children: [
      {
        path: "",
        loadChildren: () => import('./pages/main-project/main-project.module').then(m => m.MainProjectModule)
      }, {
        path: "new",
        loadChildren: () => import('./pages/new-project/new-project.module').then(m => m.NewProjectModule)
      }, {
        path: ":id",
        loadChildren: () => import('./pages/single-project/single-project.module').then(m => m.SingleProjectModule)
      }, {
        path: ":id/versions",
        loadChildren: () => import('./pages/versions-project/versions-project.module').then(m => m.VersionsProjectModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
