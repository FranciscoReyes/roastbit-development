import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import { ThemeModule } from 'src/app/theme/theme.module';
import {VersionsProjectComponent} from "./versions-project.component";
import {VersionsProjectRoutingModule} from "./versions-project-routing.module";
import {ProjectModule} from "../../project.module";

@NgModule({
  declarations: [
    VersionsProjectComponent
  ],
  imports: [
    CommonModule,
    VersionsProjectRoutingModule,
    ThemeModule,
  ]
})
export class VersionsProjectModule {}
