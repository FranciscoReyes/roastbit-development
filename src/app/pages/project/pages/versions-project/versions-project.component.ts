import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'rbit-versions-project',
  templateUrl: './versions-project.component.html',
  styleUrls: ['./versions-project.component.scss']
})
export class VersionsProjectComponent implements OnInit {

  versionsList    : Array<any>    = [];
  showChangeVersionModal : boolean = false;
  currentVersion  : number = 1.2;
  cacheVersion    : number = 0;

  constructor(private router: Router) { }

  ngOnInit() {
    this.fillFakeVersionsData();
  }
  performChangeVersion(version) {
    this.showChangeVersionModal = true;
    this.cacheVersion = version;
  }

  goProjects() {
    this.router.navigate(['pages', 'projects']).then();
  }

  changeVersion() {
    this.currentVersion = this.cacheVersion;
    this.showChangeVersionModal = false;
  }

  getIconByTag(item) : string {
    switch (item) {
      case 'success': {
        return 'e917';
      }
      case 'error': {
        return 'e919';
      }
      case 'warning': {
        return 'e918';
      }
      default: {
        return 'e91a';
      }
    }
  }

  fillFakeVersionsData() : void {
    this.versionsList = [
      {
        id: 1,
        version: 1.2,
        completed: 234,
        total: 234,
        error: 10,
        success: 23,
        warning: 14,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.3,
        completed: 234,
        total: 234,
        error: 30,
        success: 20,
        warning: 34,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.4,
        completed: 234,
        total: 234,
        error: 10,
        success: 10,
        warning: 14,
        info: 43,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.5,
        completed: 234,
        total: 234,
        error: 30,
        success: 30,
        warning: 34,
        info: 3,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.6,
        completed: 234,
        total: 234,
        error: 12,
        success: 20,
        warning: 42,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.7,
        completed: 234,
        total: 234,
        error: 10,
        success: 20,
        warning: 14,
        info: 14,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.8,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 14,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.9,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 34,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.10,
        completed: 234,
        total: 234,
        error: 10,
        success: 23,
        warning: 14,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.11,
        completed: 234,
        total: 234,
        error: 30,
        success: 20,
        warning: 34,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.12,
        completed: 234,
        total: 234,
        error: 10,
        success: 10,
        warning: 14,
        info: 43,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.13,
        completed: 234,
        total: 234,
        error: 30,
        success: 30,
        warning: 34,
        info: 3,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.14,
        completed: 234,
        total: 234,
        error: 12,
        success: 20,
        warning: 42,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.15,
        completed: 234,
        total: 234,
        error: 10,
        success: 20,
        warning: 14,
        info: 14,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.16,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 14,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.17,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 34,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },
    ];
  }

}
