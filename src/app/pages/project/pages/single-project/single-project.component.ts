import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'rbit-single-project',
  templateUrl: './single-project.component.html',
  styleUrls: ['./single-project.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SingleProjectComponent implements OnInit {

  openChat        : any           = {open: false};
  versionsList    : Array<any>    = [];
  columnDef       : Array<any>    = [];
  dataSource      : Array<any>    = [];
  typingText      : string        = null;
  ActiveItem      : any           = null;
  showPrev        : boolean       = false;
  showNext        : boolean       = true;
  currentVersion  : number        = 1.2;
  cacheVersion    : any           = null;
  showChangeVersionModal: boolean = false;

  constructor( private router: Router) { }

  ngOnInit() {
    this.fillDummyData();
    this.fillFakeColumnDef();
    this.fillFakeData();
  }

  fillFakeData() : void {
    this.dataSource = [
      {id: 123443, test: 'Validar la selección de días y horas', status: 'Pendiente', component: "Calendario", tester: 'Erubiel Recoba', creationDate: '22/08/2019 04:10', activity : {messages: 1, attachments: 4}},
      {id: 123443, test: 'Validar el formato de la fecha', status: 'Aprobada', component: "-", tester: 'Angel Damián', creationDate: '22/08/2019 04:10', extraBorder: '#4E9DC4', activity : {messages: 2, attachments: 3}},
      {id: 123443, test: 'Verificar comportamiento de zona horaria', status: 'Aprobada', component: "-", tester: 'Edgar Herrera', creationDate: '22/08/2019 04:10', extraBorder: '#E07676', activity : {messages: 4, attachments: 13}},
      {id: 123443, test: 'Compronar funcionamiento de selector por región', status: 'Cancelada', component: "-", tester: 'Dionisio Lagunes', creationDate: '22/08/2019 04:10', extraBorder: '#E07676', activity : {messages: 12, attachments: 3}},
      {id: 123443, test: 'Cargar datos masivos para comprobar carga', status: 'En proceso', component: "Tabla", tester: 'Iris de la rosa', creationDate: '22/08/2019 04:10', activity : {messages: 3, attachments: 7}},
      {id: 123443, test: 'Insertar fecha manualmente', status: 'En proceso', component: "-", tester: 'Reyna rodriguez', creationDate: '22/08/2019 04:10', activity : {messages: 11, attachments: 13}},
      {id: 123443, test: 'Validar la selección de años', status: 'En proceso', component: "Calendario", tester: 'Victor Vallejo', creationDate: '22/08/2019 04:10', extraBorder: '#AAAAAA', activity : {messages: 21, attachments: 32}}
    ];

  }

  goVersionsPage() {
    this.router.navigate(['pages', 'projects', '54876765', 'versions']).then()
  }

  performChangeVersion(version) {
    this.showChangeVersionModal = true;
    this.ActiveItem = version;
  }

  changeVersion() {
    this.currentVersion = this.cacheVersion;
    this.showChangeVersionModal = false;
    this.dataSource = this.shuffleArray(this.dataSource);
  }

  shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      let temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  fillFakeColumnDef() : void {
    this.columnDef = [
      {
        title: 'ID',
        field: 'id',
        width: 50,
      },
      {
        title: 'Prueba',
        field: 'test',
        width: 240,
      },
      {
        title: 'Estado',
        field: 'status',
        cellRenderer: (data) => {
          let status;
          switch (data.toLowerCase()) {
            case 'pendiente': {
              status = 'rejected';
            }break;
            case 'aprobada': {
              status = 'success';
            }break;
            case 'cancelada': {
              status = 'error';
            }break;
            case 'en proceso': {
              status = 'warning';
            }break;
          }

          return `
            <div class="status-container-single-project tag-${status}">
                <i class="rosbit-i rb-${this.getIconByTag(status)}"></i>
                ${data}</div>
          `;
        }
      },
      {
        title: 'Componente',
        field: 'component'
      },
      {
        title: 'Tester',
        field: 'tester'
      },
      {
        title: 'Fecha de creación',
        field: 'creationDate',
        width: 160
      },
      {
        title: 'Actividad',
        field: 'activity',
        cellRenderer: (data) => {
          return `
            <div class="d-flex direction-row activity-container-project-table align-items-center">
                <div class="d-flex item align-items-center"><i class="rosbit-i rb-e901"></i>&nbsp; ${data.messages}</div>
                <div class="d-flex item align-items-center"><i class="rosbit-i rb-e902"></i>&nbsp; ${data.attachments}</div>
            </div>
          `;
        }
      },
    ];
  }

  fillDummyData() : void {
    this.versionsList = [
      {
        id: 1,
        version: 1.2,
        completed: 234,
        total: 234,
        error: 10,
        success: 23,
        warning: 14,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.3,
        completed: 234,
        total: 234,
        error: 30,
        success: 20,
        warning: 34,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.4,
        completed: 234,
        total: 234,
        error: 10,
        success: 10,
        warning: 14,
        info: 43,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.5,
        completed: 234,
        total: 234,
        error: 30,
        success: 30,
        warning: 34,
        info: 3,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.6,
        completed: 234,
        total: 234,
        error: 12,
        success: 20,
        warning: 42,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.7,
        completed: 234,
        total: 234,
        error: 10,
        success: 20,
        warning: 14,
        info: 14,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.8,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 14,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.9,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 34,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.10,
        completed: 234,
        total: 234,
        error: 10,
        success: 23,
        warning: 14,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.11,
        completed: 234,
        total: 234,
        error: 30,
        success: 20,
        warning: 34,
        info: 13,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.12,
        completed: 234,
        total: 234,
        error: 10,
        success: 10,
        warning: 14,
        info: 43,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.13,
        completed: 234,
        total: 234,
        error: 30,
        success: 30,
        warning: 34,
        info: 3,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.14,
        completed: 234,
        total: 234,
        error: 12,
        success: 20,
        warning: 42,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.15,
        completed: 234,
        total: 234,
        error: 10,
        success: 20,
        warning: 14,
        info: 14,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.16,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 14,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },{
        id: 1,
        version: 1.17,
        completed: 234,
        total: 234,
        error: 18,
        success: 10,
        warning: 34,
        info: 33,
        lastUpdate: "3/02/20 12:32",
        counters:
          [
            {tag: 'success', count: 123},
            {tag: 'error', count: 12},
            {tag: 'warning', count: 0},
            {tag: 'rejected', count: 0},
          ]
      },
    ];
    this.ActiveItem = this.versionsList[0];
  }

  getIconByTag(item) : string {
      switch (item) {
        case 'success': {
          return 'e917';
        }
        case 'error': {
          return 'e919';
        }
        case 'warning': {
          return 'e918';
        }
        default: {
          return 'e91a';
        }
      }
  }

  toggleChat() : void {
    this.openChat['open'] = !this.openChat['open'];
  }


  scrollCarouselMore(event) {
    if (event.scrollLeft < this.versionsList.length  * 244) {
      this.showPrev = true;
      event.scrollLeft = event.scrollLeft + 244;
    }


    this.showNext = event.scrollLeft < (this.versionsList.length * 164);
  }

  scrollCarouselLess(event) {

    if (event.scrollLeft > 0) {
      this.showNext = true;
      if (event.scrollLeft < 161) {
        this.showPrev = false;
        event.scrollLeft = 0;
      } else {
        event.scrollLeft = event.scrollLeft - 244;
      }
    } else {
      this.showPrev = false;
    }
    console.log(event.scrollLeft)
  }

  fakeTyping(text: string, event) {
    this.typingText = text;
  }

  goTestDetail(event): void {
    const { id } = event;
    this.router.navigate(['pages', 'projects', '54876765', 'test', id]).then();
  }

}
