import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SingleProjectRoutingModule} from "./single-project-routing.module";
import {SingleProjectComponent} from "./single-project.component";
import { ThemeModule } from 'src/app/theme/theme.module';
import {ProjectModule} from "../../project.module";

@NgModule({
  declarations: [
    SingleProjectComponent
  ],
  imports: [
    CommonModule,
    SingleProjectRoutingModule,
    ThemeModule,
  ]
})
export class SingleProjectModule {}
