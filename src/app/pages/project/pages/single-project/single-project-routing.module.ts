import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {SingleProjectComponent} from "./single-project.component";
const routes: Routes = [
  {
    path: "",
    component: SingleProjectComponent,
  },
  {
    path: "test/:id",
    loadChildren: () => import('../../pages/test-detail-project/test-detail-project.module').then(m => m.TestDetailModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SingleProjectRoutingModule {}
