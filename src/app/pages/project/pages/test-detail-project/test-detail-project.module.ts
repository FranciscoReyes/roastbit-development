import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ThemeModule } from 'src/app/theme/theme.module';
import { TestDetailProjectRoutingModule } from './test-detail-project-routing.module';
import { TestDetailProjectComponent } from './test-detail-project.component';

@NgModule({
   declarations: [
      TestDetailProjectComponent
   ],
   imports: [
      CommonModule,
      TestDetailProjectRoutingModule,
      ThemeModule
   ]
})
export class TestDetailModule { }
