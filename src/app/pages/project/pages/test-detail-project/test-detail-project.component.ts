import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'rbit-test-detail-project',
  templateUrl: './test-detail-project.component.html',
  styleUrls: ['./test-detail-project.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TestDetailProjectComponent implements OnInit {

  columnDef: Array<any> = [];
  dataSource: Array<any> = [];
  showFormCreateNewTest = false;
  showMenu = false;

  constructor() { }

  ngOnInit() {
    this.fillFakeColumnDef();
    this.fillFakeData();
  }

  fillFakeData(): void {
    this.dataSource = [
      {
        test: '1. Entrar al login con las credenciales',
        status: 'Pendiente',
        component: {
          projectSuccessPercentage: false,
          projectErrorPercentage: false,
          projectInfoPercentage: false,
          projectWarningPercentage: false
        },
        activity: {
          messages: 1,
          attachments: 3
        }
      },
      {
        test: '2. Entrar al login con las credenciales',
        status: 'Aprobada',
        component: {
          projectSuccessPercentage: 100,
          projectErrorPercentage: false,
          projectInfoPercentage: false,
          projectWarningPercentage: false
        },
        activity: {
          messages: 1,
          attachments: 3
        }
      },
      {
        test: '3. Entrar al login con las credenciales',
        status: 'Aprobada',
        component: {
          projectSuccessPercentage: 100,
          projectErrorPercentage: false,
          projectInfoPercentage: false,
          projectWarningPercentage: false
        },
        activity: {
          messages: 1,
          attachments: 3
        }
      },
      {
        test: '4. Entrar al login con las credenciales',
        status: 'Cancelada',
        component: {
          projectSuccessPercentage: false,
          projectErrorPercentage: 100,
          projectInfoPercentage: false,
          projectWarningPercentage: false
        },
        activity: {
          messages: 1,
          attachments: 3
        }
      },
      {
        test: '5. Entrar al login con las credenciales',
        status: 'En proceso',
        component: {
          projectSuccessPercentage: false,
          projectErrorPercentage: false,
          projectInfoPercentage: false,
          projectWarningPercentage: 70
        },
        activity: {
          messages: 1,
          attachments: 3
        }
      },
      {
        test: '6. Entrar al login con las credenciales',
        status: 'En proceso',
        component: {
          projectSuccessPercentage: false,
          projectErrorPercentage: false,
          projectInfoPercentage: false,
          projectWarningPercentage: 40
        },
        activity: {
          messages: 1,
          attachments: 3
        }
      },
      {
        test: '7. Entrar al login con las credenciales',
        status: 'En proceso',
        component: {
          projectSuccessPercentage: false,
          projectErrorPercentage: false,
          projectInfoPercentage: false,
          projectWarningPercentage: 40
        },
        activity: {
          messages: 1,
          attachments: 3
        }
      }
    ];
  }


  fillFakeColumnDef(): void {
    this.columnDef = [
      {
        title: '',
        field: 'test',
        width: 240,
      },
      {
        title: '',
        field: 'status',
        cellRenderer: (data) => {
          let status;
          switch (data.toLowerCase()) {
            case 'pendiente': {
              status = 'rejected';
            } break;
            case 'aprobada': {
              status = 'success';
            } break;
            case 'cancelada': {
              status = 'error';
            } break;
            case 'en proceso': {
              status = 'warning';
            } break;
          }

          return `
            <div class="status-container">
              <div class="status-container-single-project tag-${status}">
                <i class="rosbit-i rb-${this.getIconByTag(status)}"></i>
                ${data}
              </div>
            </div>
          `;
        }
      },
      {
        title: '',
        field: 'component',
        cellRenderer: (data) => {
          if (data.projectSuccessPercentage) {
            return `
            <div class="progress-container" [style.paddingTop.px]="7" [style.paddingBottom.px]="7">
              <div class="d-project-progress">
                <div *ngIf="${data.projectSuccessPercentage}" class="d-progress-bar-success" style="width: ${data.projectSuccessPercentage}%"
                  [attr.aria-valuenow]="${data.projectSuccessPercentage}" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="d-project-percentage-container">
                <p class="d-project-percentage">${data.projectSuccessPercentage}%</p>
              </div>
            </div>
            `;
          } else if (data.projectErrorPercentage) {
            return `
            <div class="progress-container" [style.paddingTop.px]="7" [style.paddingBottom.px]="7">
              <div class="d-project-progress">
                <div *ngIf="${data.projectErrorPercentage}" class="d-progress-bar-error" style="width: ${data.projectErrorPercentage}%"
                  [attr.aria-valuenow]="${data.projectErrorPercentage}" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="d-project-percentage-container">
                <p class="d-project-percentage">0%</p>
              </div>
            </div>
            `;
          }
          else if (data.projectInfoPercentage) {
            return `
            <div class="progress-container" [style.paddingTop.px]="7" [style.paddingBottom.px]="7">
              <div class="d-project-progress">
                <div *ngIf="${data.projectInfoPercentage}" class="d-progress-bar-info" style="width: ${data.projectInfoPercentage}%"
                  [attr.aria-valuenow]="${data.projectInfoPercentage}" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="d-project-percentage-container">
                <p class="d-project-percentage">${data.projectInfoPercentage}%</p>
              </div>
            </div>
            `;
          }
          else if (data.projectWarningPercentage) {
            return `
            <div class="progress-container" [style.paddingTop.px]="7" [style.paddingBottom.px]="7">
              <div class="d-project-progress">
                <div *ngIf="${data.projectWarningPercentage}" class="d-progress-bar-warning" style="width: ${data.projectWarningPercentage}%"
                  [attr.aria-valuenow]="${data.projectWarningPercentage}" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="d-project-percentage-container">
                <p class="d-project-percentage">${data.projectWarningPercentage}%</p>
              </div>
            </div>
            `;
          } else {
            return `
            <div class="progress-container" [style.paddingTop.px]="7" [style.paddingBottom.px]="7">
              <div class="d-project-progress">
                <div class="d-progress-bar-warning" style="width: 0%"
                  [attr.aria-valuenow]="0" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="d-project-percentage-container">
                <p class="d-project-percentage">0%</p>
              </div>
            </div>
            `;
          }
        }
      },
      {
        title: '',
        field: 'activity',
        width: 100,
        cellRenderer: (data) => {
          return `
            <div class="d-flex direction-row activity-container-project-table align-items-center">
                <div class="d-flex item align-items-center margin-right-10"><i class="rosbit-i rb-e901"></i>&nbsp; ${data.messages}</div>
                <div class="d-flex item align-items-center"><i class="rosbit-i rb-e902"></i>&nbsp; ${data.attachments}</div>
            </div>
          `;
        }
      },
    ];
  }

  getIconByTag(item): string {
    switch (item) {
      case 'success': {
        return 'e917';
      }
      case 'error': {
        return 'e919';
      }
      case 'warning': {
        return 'e918';
      }
      default: {
        return 'e91a';
      }
    }
  }

  createNewTest() {
    this.showFormCreateNewTest = true;
  }

  cancelNewTest() {
    this.showFormCreateNewTest = false;
  }

  goTestDetail(event?) {
    this.showMenu = true;
  }

  hideMenu() {
    this.showMenu = false;
  }

}
