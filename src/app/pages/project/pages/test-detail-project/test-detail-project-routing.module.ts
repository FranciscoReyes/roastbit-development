import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TestDetailProjectComponent } from "./test-detail-project.component";

const routes: Routes = [
   {
      path: "",
      component: TestDetailProjectComponent,
   }
];

@NgModule({
   imports: [RouterModule.forChild(routes)],
   exports: [RouterModule]
})

export class TestDetailProjectRoutingModule { }
