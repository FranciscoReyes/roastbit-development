import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDetailProjectComponent } from './test-detail-project.component';

describe('TestDetailProjectComponent', () => {
  let component: TestDetailProjectComponent;
  let fixture: ComponentFixture<TestDetailProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestDetailProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestDetailProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
