import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MainProjectRoutingModule} from "./main-project-routing.module";
import {MainProjectComponent} from "./main-project.component";
import {PProjectCardComponent} from "../../components/p-project-card/p-project-card.component";
import {PProjectAddTestComponent} from "../../components/p-proyect-add-test/p-project-add-test.component";
import {DProjectCardsComponent} from "../../components/d-project-card/d-project-card.component";
import { DAvatarListComponent } from '../../../../theme/components/avatar/d-avatar-list.component';
import { DProjectCardModule } from 'src/app/pages/dashboard/components/d-project-card/d-project-card.module';
import {FormsModule} from "@angular/forms";
import {ThemeModule} from "../../../../theme/theme.module";

@NgModule({
  declarations: [
    MainProjectComponent,
    PProjectCardComponent,
    DProjectCardsComponent,
    DAvatarListComponent,
    PProjectAddTestComponent
  ],
  imports: [
    CommonModule,
    MainProjectRoutingModule,
    DProjectCardModule,
    FormsModule,
    ThemeModule
  ]
})
export class MainProjectModule {}
