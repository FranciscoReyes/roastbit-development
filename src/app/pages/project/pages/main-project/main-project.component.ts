import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'main-project-page',
  templateUrl: 'main-project.component.html',
  styleUrls: ['main-project.component.scss']
})
export class MainProjectComponent implements OnInit {

  showAddTest:boolean = false;
  constructor(
    private router : Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe({
      next: (params)=>{
        if (params.addTest == "true") {
          this.showAddTest = true;
          setTimeout(()=>{
            this.router.navigate(["pages", "projects"]).then();
          }, 1500);
        }
      }
    });
  }

  ngOnInit() {
  }

  goProjectDetail() : void {
    this.router.navigate(['pages', 'projects', '54876765']).then();
  }

  onCloseAddTest(event){
    this.showAddTest = false;
  }
}
