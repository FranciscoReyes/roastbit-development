import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as noUiSlider from 'nouislider';
import { Location } from '@angular/common';
import {Router} from "@angular/router";
@Component({
  selector: 'rbit-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewProjectComponent implements OnInit {


  isOnForm: boolean = true;
  projectTypes : Array<any> = [];
  currentStep : number = 1;

  constructor(
    private location: Location,
    private router : Router
  ) {}

  ngOnInit() {
    this.projectTypes = [
      {id: 1, name: "Aplicación móvil"},
      {id: 2, name: "Página web"},
      {id: 3, name: "Plataforma"},
      {id: 4, name: "API"},
      {id: 5, name: "Otro"},
    ];
    setTimeout(() => {
      this.setHandlerSlider()
    }, 300)
  }

  nextStep() {
    if (this.currentStep == 2) {
      this.createProject();
    }
    this.currentStep++;
  }

  backStep() {
    this.setSecondSlier(0);
    this.currentStep--;
  }

  markAsActive(selected){
    this.projectTypes.forEach(item=>{
      item.active = item.id == selected.id;
    });
  }

  nextStepTimer() {
    this.setSecondSlier(1);
    setTimeout(()=>this.nextStep(), 400)
  }

  setSecondSlier(number) {
    var handlSlider : any = document.getElementsByClassName('slider-project')[0];
    handlSlider.noUiSlider.set(number)
  }

  setHandlerSlider () {
    var handlSlider = document.getElementsByClassName('slider-project')[0];
    noUiSlider.create(handlSlider, {
      animate: true,
      animationDuration: 300,
      start: [0, 2],
      connect: [true, false, false],
      step: 1,
      range: {
          'min': [0],
          'max': [1]
      }
    })
  }

  createProject() {
    this.isOnForm = false;
  }

  cancel() {
    this.currentStep = 1;
    this.location.back();
  }

  createTest() {
    this.router.navigate(['pages', 'projects'], {queryParams: {addTest:true}}).then();
  }
}
