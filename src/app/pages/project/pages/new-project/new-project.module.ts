import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {NewProjectRoutingModule} from "./new-project-routing.module";
import {NewProjectComponent} from "./new-project.component";
import { ThemeModule } from 'src/app/theme/theme.module';

@NgModule({
  declarations: [
    NewProjectComponent
  ],
  imports: [
    CommonModule,
    NewProjectRoutingModule,
    ThemeModule
  ]
})
export class NewProjectModule {}
