import {NgModule} from "@angular/core";
import {LoginPage} from "./login.component";
import {Routes, RouterModule} from "@angular/router";

const routes : Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}
