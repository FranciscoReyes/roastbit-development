import {AfterViewInit, Component, OnInit} from '@angular/core';

@Component({
  selector: 'login-page',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginPage implements OnInit, AfterViewInit {

  showTriangle1 = false;
  showTriangle2 = false;
  showTriangle3 = false;
  showTriangle4 = false;
  showTriangle5 = false;
  showTriangle6 = false;
  showTriangle7 = false;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(()=>this.showTriangle1 = true, 1000);
    setTimeout(()=>this.showTriangle2 = true, 1100);
    setTimeout(()=>this.showTriangle3 = true, 1200);
    setTimeout(()=>this.showTriangle4 = true, 1300);
    setTimeout(()=>this.showTriangle5 = true, 1500);
    setTimeout(()=>this.showTriangle6 = true, 1600);
    setTimeout(()=>this.showTriangle7 = true, 1700);
  }

}
