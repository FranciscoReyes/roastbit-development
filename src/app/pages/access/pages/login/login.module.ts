import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPage } from './login.component';
import {ThemeModule} from "../../../../theme/theme.module";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {LoginRoutingModule} from "./login-routing.module";

const CORE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  ThemeModule,

  LoginRoutingModule
];

const THIRD_PARTY_MODULES = [
  // TranslateModule,
  // NgxPermissionsModule
];

@NgModule({
  declarations: [
    LoginPage
  ],
  imports: [
    ...CORE_MODULES,
    ...THIRD_PARTY_MODULES
  ]
})
export class LoginModule { }
