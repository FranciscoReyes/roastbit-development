import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {ThemeModule} from "../../theme/theme.module";
import {AccessRoutingModule} from "./access-routing.module";
import {AccessComponent} from "./access.component";

const CORE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  ThemeModule,

  AccessRoutingModule
];

const THIRD_PARTY_MODULES = [
  // TranslateModule,
  // NgxPermissionsModule
];

@NgModule({
  declarations: [
    AccessComponent
  ],
  imports: [
    ...CORE_MODULES,
    ...THIRD_PARTY_MODULES
  ],
  providers: []
})
export class AccessModule {}
