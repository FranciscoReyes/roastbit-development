import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {DashboardComponent} from "./dashboard.component";

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: "test",
        loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: "client",
        loadChildren: () => import('./pages/main-dashboard/main-dashboard.module').then(m => m.MainDashboardModule)
      },
      {
        path: "testers",
        loadChildren: () => import('./pages/testers-dashboard/testers-dashboard.module').then(m => m.TestersDashboardModule)
      },
      {
        path: "admin",
        loadChildren: () => import('./pages/admin-dashboard/admin-dashboard.module').then(m => m.adminDashboardModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
