import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import {DashboardComponent} from "./dashboard.component";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import { ThemeModule } from '../../theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const CORE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  DashboardRoutingModule,
  ThemeModule,
];

@NgModule({
  declarations: [
    DashboardComponent,
  ],
  imports: [
    ...CORE_MODULES,
    DashboardRoutingModule
  ]
})
export class DashboardModule {}
