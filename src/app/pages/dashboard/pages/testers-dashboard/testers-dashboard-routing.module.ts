import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {TestersDashboardComponent} from "./testers-dashboard.component";

const routes: Routes = [
  {
    path: '',
    component: TestersDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestersDashboardRoutingModule {}
