import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestersDashboardComponent } from './testers-dashboard.component';

describe('TestersDashboardComponent', () => {
  let component: TestersDashboardComponent;
  let fixture: ComponentFixture<TestersDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestersDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestersDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});