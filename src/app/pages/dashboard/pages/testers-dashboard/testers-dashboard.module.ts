import {NgModule} from "@angular/core";
import {TestersDashboardComponent} from "./testers-dashboard.component";
import {CommonModule} from "@angular/common";
import {TestersDashboardRoutingModule} from "./testers-dashboard-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {GridsterModule} from "angular-gridster2";
import {ThemeModule} from "../../../../theme/theme.module";
import { DAnalysisIndicatorModule } from '../../components/d-analysis-indicator/d-analysis-indicator.module';
import { DProjectCardModule } from '../../components/d-project-card/d-project-card.module';
import { DTestingCardModule } from '../../components/d-testing-card/d-testing-card.module';
import { DAmchartsTestComponent } from '../../components/d-amcharts-test/d-amcharts-test.component';
import { DAvatarCardHeaderComponentModule } from '../../components/d-avatar-card-header/d-avatar-card-header.component.module';
import { CommonComponentsModule } from '../../components/commonComponents.module';

const CORE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  TestersDashboardRoutingModule,
  ThemeModule,
  DAnalysisIndicatorModule,
  DAvatarCardHeaderComponentModule,
  CommonComponentsModule,
  DProjectCardModule,
  DTestingCardModule
];
const THIRD_PARTY_MODULES = [
  GridsterModule
];

@NgModule({
  declarations: [
    TestersDashboardComponent,
    DAmchartsTestComponent,
  ],
  imports: [
    ...CORE_MODULES,
    ...THIRD_PARTY_MODULES,
  ]
})
export class TestersDashboardModule { }
