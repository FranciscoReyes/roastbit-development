import { Component, OnInit, AfterViewInit, HostListener, ElementRef, NgZone } from '@angular/core';
import { UUID } from "angular2-uuid";
import { GridsterConfig } from "angular-gridster2";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";

@Component({
  selector: 'admin-dashboard-page',
  templateUrl: 'admin-dashboard.component.html',
  styleUrls: ['admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit, AfterViewInit {
  private chart: am4charts.XYChart;

  options: GridsterConfig = {
    draggable: {
      enabled: false
    },
    pushItems: false,
    resizable: {
      enabled: false
    },
    margin: 25,
    outerMargin: false,
    fixedRowHeight: 32.5,
    mobileBreakpoint: 701,//+170

    //gridType: "scrollVertical",
  };

  buttonGrid: boolean = false;

  isOnMobileMode: boolean = false;

  gridsterItem1 = {
    cols: 3,
    id: UUID.UUID(),
    rows: 1.5,
    x: 0,
    y: 0,
  };
  gridsterItem2 = {
    cols: 2,
    id: UUID.UUID(),
    rows: 1.5,
    x: 3,
    y: 0
  };
  gridsterItem3 = {
    cols: 2,
    id: UUID.UUID(),
    rows: 1.5,
    x: 5,
    y: 0
  };
  gridsterItem4 = {
    cols: 2,
    id: UUID.UUID(),
    rows: 1.5,
    x: 7,
    y: 0
  };
  gridsterItem5 = {
    cols: 4,
    id: UUID.UUID(),
    rows: 12,
    x: 9,
    y: 0
  };

  gridsterItem6 = {
    cols: 3,
    id: UUID.UUID(),
    rows: 6,
    x: 0,
    y: 1.6
  };

  gridsterItem7 = {
    cols: 6,
    id: UUID.UUID(),
    rows: 6,
    x: 3,
    y: 1.6
  };
  gridsterItem8 = {
    cols: 6,
    id: UUID.UUID(),
    rows: 4,
    x: 0,
    y: 8
  };

  chipsFilterPerformance = [{ name: "Proyecto 1", color: '#AB195E' }, { name: "Proyecto 2", color: '#D90F16' }, { name: "Proyecto 3", color: '#B90B13' }, { name: "Proyecto 4", color: '#D90F16' }, { name: "Proyecto 5", color: '#E8397A' },];


  // layout: GridsterItem[] = [
  // ]

  constructor(
    private elementRef: ElementRef,
    private _zone: NgZone
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.detectScreenSize();
    this.getChart();
    this.getPie();
    this.getPie2();
    this.getPie3();
  }

  detectScreenSize(event?: any) {
    let size;
    if (!event) size = window.innerWidth;
    else size = event.target.innerWidth;
    this.isOnMobileMode = size <= 871;// 871 is our mobile breakpoint
  }

  @HostListener("window:resize", ['$event'])
  onResize(event) {
    console.log('event', event)
    this.detectScreenSize(event);
  }
  getChart() {
    this._zone.runOutsideAngular(() => {
      let chartP = am4core.create("chartPerformance", am4charts.XYChart);
      chartP.data = [
        {
          "day": "Lunes",
          "ejecutadas": 2.5,
          "no ejecutadas": 0.2,
        },
        {
          "day": "Martes",
          "ejecutadas": 2.5,
          "no ejecutadas": 0.2,
        },
        {
          "day": "Miércoles",
          "ejecutadas": 2.5,
          "no ejecutadas": 0.2,
        },
        {
          "day": "Jueves",
          "ejecutadas": 2.5,
          "no ejecutadas": 0.2,
        },
        {
          "day": "Viernes",
          "ejecutadas": 0.6,
          "no ejecutadas": 2.7,
        },
        {
          "day": "Sábado",
          "ejecutadas": 0.8,
          "no ejecutadas": 2,
        },
        {
          "day": "Domingo",
          "ejecutadas": 0.2,
          "no ejecutadas": 2,
        }
      ];

      // Create axes
      let categoryAxis = chartP.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "day";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 20;
      categoryAxis.renderer.cellStartLocation = 0.1;
      categoryAxis.renderer.cellEndLocation = 0.9;

      let valueAxis = chartP.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;

      // Create series
      function createSeries(field, name, stacked) {
        let series = chartP.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "day";
        series.name = name;

        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = stacked;
        series.columns.template.width = am4core.percent(10);
        //series1.columns.template.width = am4core.percent(10);
        series.columns.template.fill = field === 'ejecutadas' ? am4core.color("#C54338") : am4core.color("#DA9219");
        series.columns.template.stroke = field === 'ejecutadas' ? am4core.color("#C54338") : am4core.color("#DA9219");
        let columnTemplate = series.columns.template;
        columnTemplate.width = 30;
        columnTemplate.column.cornerRadiusTopLeft = field === 'ejecutadas' ? 0 : 30;
        columnTemplate.column.cornerRadiusTopRight = field === 'ejecutadas' ? 0 : 30;
        columnTemplate.strokeOpacity = 0;
      }
      createSeries("ejecutadas", "Ejecutadas", false);
      createSeries("no ejecutadas", "No Ejecutadas", true);

      // Add legend
      chartP.legend = new am4charts.Legend();
      chartP.legend.marginLeft = 0;
      chartP.legend.position = "top"
      this.chart = chartP;
    });
  }

  getPie() {
    this._zone.runOutsideAngular(() => {
      let chartP = am4core.create('chartPerf', am4charts.PieChart);
      chartP.data = [
        {
          "country": "Falta",
          "litres": 66
        },
        {
          "country": "Plataforma",
          "litres": 34
        },
      ];
      // Add label
      chartP.innerRadius = 30;
      let label = chartP.seriesContainer.createChild(am4core.Label);
      label.text = (((chartP.data[1].litres/ (chartP.data[0].litres + chartP.data[1].litres ))*100)).toPrecision(2)+"%" ;
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize= 10;

      // Add and configure Series
      let pieSeries = chartP.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#f2f2f2"),
        am4core.color("#DA9219"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.value = "litres";
      pieSeries.dataFields.category = "country";
    });
  }
  getPie2() {
    this._zone.runOutsideAngular(() => {
      let chartP = am4core.create('chartPerf2', am4charts.PieChart);
      chartP.data = [
        {
          "country": "Falta",
          "litres": 84
        },
        {
          "country": "Página web",
          "litres": 16
        },
      ];
      // Add label
      chartP.innerRadius = 40;
      let label = chartP.seriesContainer.createChild(am4core.Label);
      label.text = (((chartP.data[1].litres/ (chartP.data[0].litres + chartP.data[1].litres ))*100)).toPrecision(2)+"%" ;
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize= 10;

      // Add and configure Series
      let pieSeries = chartP.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#f2f2f2"),
        am4core.color("#C54338"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.value = "litres";
      pieSeries.dataFields.category = "country";
    });
  }
  getPie3() {
    this._zone.runOutsideAngular(() => {
      let chartP = am4core.create('chartPerf3', am4charts.PieChart);
      chartP.data = [
        {
          "country": "Falta",
          "litres": 18
        },
        {
          "country": "Aplicación Móvil",
          "litres": 82
        },
      ];
      // Add label
      chartP.innerRadius = 40;
      let label = chartP.seriesContainer.createChild(am4core.Label);
      label.text = (((chartP.data[1].litres/ (chartP.data[0].litres + chartP.data[1].litres ))*100)).toPrecision(2)+"%" ;
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize= 10;

      // Add and configure Series
      let pieSeries = chartP.series.push(new am4charts.PieSeries());
      pieSeries.colors.list = [
        am4core.color("#f2f2f2"),
        am4core.color("#B90B13"),
      ];
      pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.disabled = true;
      pieSeries.dataFields.value = "litres";
      pieSeries.dataFields.category = "country";

      // chartP.legend = new am4charts.Legend();
      // chartP.legend.position = "right"
    });
  }
}
/*
// Create series
      let series1 = chart.series.push(new am4charts.RadarColumnSeries());
      series1.dataFields.valueX = "full";
      series1.dataFields.categoryY = "category";
      series1.clustered = false;
      series1.columns.template.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
      series1.columns.template.fillOpacity = 0.08;
      series1.columns.template.cornerRadiusTopLeft = 20;
      series1.columns.template.strokeWidth = 0;
      series1.columns.template.radarColumn.cornerRadius = 20;

      let series2 = chart.series.push(new am4charts.RadarColumnSeries());
      series2.dataFields.valueX = "value";
      series2.dataFields.categoryY = "category";
      series2.clustered = false;
      series2.columns.template.strokeWidth = 0;
      series2.columns.template.radarColumn.cornerRadius = 20;
*/
