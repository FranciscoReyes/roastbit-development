import {NgModule} from "@angular/core";
import {AdminDashboardComponent} from "./admin-dashboard.component";
import {CommonModule} from "@angular/common";
import {adminDashboardRoutingModule} from "./admin-dashboard-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {GridsterModule} from "angular-gridster2";
import {ThemeModule} from "../../../../theme/theme.module";
import { DAnalysisIndicatorModule } from '../../components/d-analysis-indicator/d-analysis-indicator.module';
import { DProjectCardModule } from '../../components/d-project-card/d-project-card.module';
import { DAvatarCardHeaderComponentModule } from '../../components/d-avatar-card-header/d-avatar-card-header.component.module';
import { CommonComponentsModule } from '../../components/commonComponents.module';

const CORE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  adminDashboardRoutingModule,
  ThemeModule,
  DAnalysisIndicatorModule,
  DAvatarCardHeaderComponentModule,
  DProjectCardModule,
  CommonComponentsModule,
];
const THIRD_PARTY_MODULES = [
  GridsterModule
];

@NgModule({
  declarations: [
    AdminDashboardComponent,
  ],
  imports: [
    ...CORE_MODULES,
    ...THIRD_PARTY_MODULES,
  ]
})
export class adminDashboardModule { }
