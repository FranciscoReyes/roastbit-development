import {Component, OnInit, AfterViewInit, HostListener, ElementRef, NgZone} from '@angular/core';
import {UUID} from "angular2-uuid";
import {GridsterConfig} from "angular-gridster2";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";

@Component({
  selector: 'main-dashboard-page',
  templateUrl: 'main-dashboard.component.html',
  styleUrls: ['main-dashboard.component.scss']
})
export class MainDashboardComponent implements OnInit, AfterViewInit {
  private chart: am4charts.XYChart;

  options: GridsterConfig = {
    draggable: {
      enabled: false
    },
    pushItems: false,
    resizable: {
      enabled: false
    },
    margin: 25,
    outerMargin: false,
    fixedRowHeight: 32.5,
    mobileBreakpoint: 701,//+170

    //gridType: "scrollVertical",
  };

  isOnMobileMode : boolean = false;

  gridsterItem1 = {
    cols: 3,
    id: UUID.UUID(),
    rows: 2,
    x: 0,
    y: 0,
  };
  gridsterItem2 = {
    cols: 2,
    id: UUID.UUID(),
    rows: 2,
    x: 3,
    y: 0
  };
  gridsterItem3 = {
    cols: 2,
    id: UUID.UUID(),
    rows: 2,
    x: 5,
    y: 0
  };
  gridsterItem4 = {
    cols: 2,
    id: UUID.UUID(),
    rows: 2,
    x: 7,
    y: 0
  };
  gridsterItem5 = {
    cols: 2,
    id: UUID.UUID(),
    rows: 2,
    x: 9,
    y: 0
  };
  gridsterItem6 = {
    cols: 3,
    id: UUID.UUID(),
    rows: 8,
    x: 0,
    y: 2
  };
  gridsterItem7 = {
    cols: 3,
    id: UUID.UUID(),
    rows: 8,
    x: 3,
    y: 2
  };
  gridsterItem8 = {
    cols: 5,
    id: UUID.UUID(),
    rows: 5,
    x: 5,
    y: 2
  };

  gridsterItem9 = {
    cols: 5,
    id: UUID.UUID(),
    rows: 3,
    x: 5,
    y: 6
  };

  chipsFilterPerformance = [{name: "Proyecto 1", color: '#AB195E'},{name: "Proyecto 2", color: '#D90F16'},{name: "Proyecto 3", color: '#B90B13'},{name: "Proyecto 4", color: '#D90F16'},{name: "Proyecto 5", color: '#E8397A'},];


  // layout: GridsterItem[] = [
  // ]

  constructor(
    private elementRef: ElementRef,
    private _zone: NgZone
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.detectScreenSize();
    this.getChart();
  }

  detectScreenSize(event?: any) {
    let size;

    if (!event) {
      size = window.innerWidth;
    } else {
      size = event.target.innerWidth;
    }

    this.isOnMobileMode = size <= 871;// 871 is our mobile breakpoint
  }

  @HostListener("window:resize", ['$event'])
  onResize(event) {
    this.detectScreenSize(event);
  }
  getChart(){
    this._zone.runOutsideAngular(() => {
      let chartP = am4core.create("chartPerformance", am4charts.XYChart);

      // Add data
      chartP.data = [{
        "x": 1,
        "ay": 6.5,
        "by": 2.2,
        "cx": 15,
        "cy": 2.5,
        "aValue": 15,
        "bValue": 10,
        "cValue": 5
      }, {
        "x": 2,
        "ay": 12.3,
        "by": 4.9,
        "cx": 16,
        "cy": 2.3,
        "aValue": 8,
        "bValue": 3,
        "cValue": 6
      }, {
        "x": 3,
        "ay": 12.3,
        "by": 5.1,
        "cx": 10,
        "cy": 2,
        "aValue": 16,
        "bValue": 4,
        "cValue": 7
      }, {
        "x": 5,
        "ay": 2.9,
        "cx": 19,
        "cy": 2,
        "aValue": 9,
        "cValue": 7
      }, {
        "x": 7,
        "by": 8.3,
        "cx": 19,
        "cy": 2,
        "bValue": 13,
        "cValue": 7
      }, {
        "x": 10,
        "ay": 2.8,
        "by": 13.3,
        "cx": 19,
        "cy": 2,
        "aValue": 9,
        "bValue": 13,
        "cValue": 7
      }, {
        "x": 12,
        "ay": 3.5,
        "by": 6.1,
        "cx": 19,
        "cy": 2,
        "aValue": 5,
        "bValue": 2,
        "cValue": 7
      }, {
        "x": 13,
        "ay": 5.1,
        "cx": 19,
        "cy": 2,
        "aValue": 10,
        "cValue": 9
      }, {
        "x": 15,
        "ay": 6.7,
        "by": 10.5,
        "cx": 19,
        "cy": 2,
        "aValue": 3,
        "bValue": 10,
        "cValue": 6
      }, {
        "x": 16,
        "ay": 8,
        "by": 12.3,
        "cx": 17,
        "cy": 1.7,
        "aValue": 5,
        "bValue": 13,
        "cValue": 7
      }, {
        "x": 20,
        "by": 4.5,
        "cx": 17,
        "cy": 1.7,
        "bValue": 11,
        "cValue": 7
      }, {
        "x": 22,
        "ay": 9.7,
        "by": 15,
        "cx": 17,
        "cy": 1.7,
        "aValue": 15,
        "bValue": 10,
        "cValue": 12
      }, {
        "x": 23,
        "ay": 10.4,
        "by": 10.8,
        "cx": 17,
        "cy": 1.7,
        "aValue": 1,
        "bValue": 11,
        "cValue": 5
      }, {
        "x": 24,
        "ay": 1.7,
        "by": 19,
        "cx": 1.7,
        "cy": 1.7,
        "aValue": 12,
        "bValue": 3,
        "cValue": 10
      }];

// Create axes
      var xAxis = chartP.xAxes.push(new am4charts.ValueAxis());
      xAxis.renderer.minGridDistance = 40;

// Create value axis
      var yAxis = chartP.yAxes.push(new am4charts.ValueAxis());

// Create series
      var series1 = chartP.series.push(new am4charts.LineSeries());
      series1.dataFields.valueX = "x";
      series1.dataFields.valueY = "ay";
      series1.dataFields.value = "aValue";
      series1.strokeWidth = 2;
      series1.fill = am4core.color("#DA9219");
      series1.stroke = am4core.color("#DA9219");

      var series2 = chartP.series.push(new am4charts.LineSeries());
      series2.dataFields.valueX = "x";
      series2.dataFields.valueY = "by";
      series2.dataFields.value = "bValue";
      series2.strokeWidth = 2;
      series2.fill = am4core.color("#C54338");
      series2.stroke = am4core.color("#C54338");

      var series3 = chartP.series.push(new am4charts.LineSeries());
      series3.dataFields.valueX = "x";
      series3.dataFields.valueY = "by";
      series3.dataFields.value = "bValue";
      series3.strokeWidth = 2;
      series3.fill = am4core.color("#B90B13");
      series3.stroke = am4core.color("#B90B13");

      var series4 = chartP.series.push(new am4charts.LineSeries());
      series4.dataFields.valueX = "cx";
      series4.dataFields.valueY = "cy";
      series4.dataFields.value = "cValue";
      series4.strokeWidth = 2;
      series4.fill = am4core.color("#C54338");
      series4.stroke = am4core.color("#C54338");

      var series5 = chartP.series.push(new am4charts.LineSeries());
      series5.dataFields.valueX = "aValue";
      series5.dataFields.valueY = "cValue";
      series5.dataFields.value = "by";
      series5.strokeWidth = 2;
      series5.fill = am4core.color("#C44F80");
      series5.stroke = am4core.color("#C44F80");

      this.chart = chartP;

    });
  }
}
