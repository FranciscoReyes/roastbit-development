import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DAnalysisIndicatorContentComponent } from './d-analysis-indicator.component';

describe('DAnalysisIndicatorContentComponent', () => {
  let component: DAnalysisIndicatorContentComponent;
  let fixture: ComponentFixture<DAnalysisIndicatorContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DAnalysisIndicatorContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DAnalysisIndicatorContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
