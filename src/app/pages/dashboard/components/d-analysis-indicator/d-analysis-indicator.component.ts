import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'd-analysis-indicator',
  templateUrl: 'd-analysis-indicator.component.html',
  styleUrls: ['d-analysis-indicator.component.scss'],
  host: {
    "class": "d-analysis-full-cover",
    "[attr.width.%]": "100",
    "[attr.height.%]": "100"
  }
})
export class DAnalysisIndicatorComponent implements OnInit {

  @Input() short : boolean = false;
  @Input() backgroundColor : 'yellow'| 'red' | 'default' = 'default';
  @Input() fontIcon : string;
  @Input() colorIcon : 'blue' | 'green' | 'gray' | 'red' | 'black' | 'yellow' = 'black';
  @Input() number : number;
  @Input() secondaryNumber : number;
  @Input() text : string;
  @Input() hasProgressBar : boolean = false;
  @Input() percentageProgressBar : number = 0;

  constructor() {
    
  }

  ngOnInit() {
  }

  getType(){
    return !this.fontIcon? 'analysis-content-no-icon': 'analysis-content'
  }
}
