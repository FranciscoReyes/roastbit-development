import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {DAnalysisIndicatorComponent} from './d-analysis-indicator.component'

@NgModule({
    imports: [CommonModule, FormsModule],
    exports: [DAnalysisIndicatorComponent],
    declarations: [DAnalysisIndicatorComponent]
})
export class DAnalysisIndicatorModule {}