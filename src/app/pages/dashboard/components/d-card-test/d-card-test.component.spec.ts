import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DCardTestComponent } from './d-card-test.component';

describe('DCardTestComponent', () => {
  let component: DCardTestComponent;
  let fixture: ComponentFixture<DCardTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DCardTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DCardTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
