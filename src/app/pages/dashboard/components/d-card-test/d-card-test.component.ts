import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rbit-d-card-test',
  templateUrl: './d-card-test.component.html',
  styleUrls: ['./d-card-test.component.scss']
})
export class DCardTestComponent implements OnInit {
  @Input() backgroundColor : 'yellow' | 'blue' | 'green' | 'gray' | 'red' | 'black' = 'black';
  
  constructor() { }

  ngOnInit() {
  }

}
