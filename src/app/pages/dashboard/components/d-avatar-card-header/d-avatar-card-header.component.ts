import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rbit-d-avatar-card-header',
  templateUrl: './d-avatar-card-header.component.html',
  styleUrls: ['./d-avatar-card-header.component.scss']
})
export class DAvatarCardHeaderComponent implements OnInit {
  @Input() developerName: string;
  @Input() developerBullets: number;
  @Input() imgTesting: string;
  @Input() developerPoints: number = 0;
  @Input() BgColor: "yellow"| "red" = "yellow"; 
  @Input() developersView:boolean = false; 
  @Input() textView: string = "";
  @Input() hour: string = "";

  constructor() { }

  ngOnInit() {
  }

  getType(){
    return this.BgColor === 'yellow' ?'bullets yellow':'bullets red';
  }

}
