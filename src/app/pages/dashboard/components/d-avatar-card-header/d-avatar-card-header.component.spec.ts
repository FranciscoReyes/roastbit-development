import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DAvatarCardHeaderComponent } from './d-avatar-card-header.component';

describe('DAvatarCardHeaderComponent', () => {
  let component: DAvatarCardHeaderComponent;
  let fixture: ComponentFixture<DAvatarCardHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DAvatarCardHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DAvatarCardHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
