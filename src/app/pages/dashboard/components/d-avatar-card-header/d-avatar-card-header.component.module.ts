import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {DAvatarCardHeaderComponent} from './d-avatar-card-header.component'

@NgModule({
    imports: [CommonModule, FormsModule],
    exports: [DAvatarCardHeaderComponent],
    declarations: [DAvatarCardHeaderComponent]
})
export class DAvatarCardHeaderComponentModule {}