import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DAmchartsTestComponent } from './d-amcharts-test.component';

describe('DAmchartsTestComponent', () => {
  let component: DAmchartsTestComponent;
  let fixture: ComponentFixture<DAmchartsTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DAmchartsTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DAmchartsTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
