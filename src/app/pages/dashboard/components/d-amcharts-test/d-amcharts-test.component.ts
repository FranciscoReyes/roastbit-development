import { Component, NgZone, OnDestroy, ViewContainerRef, HostListener, ViewChild, Input } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { HttpClient } from '@angular/common/http';
am4core.useTheme(am4themes_animated);


@Component({
  selector: 'rbit-d-amcharts-test',
  templateUrl: './d-amcharts-test.component.html',
  styleUrls: ['./d-amcharts-test.component.scss']
})
export class DAmchartsTestComponent  {
  @Input() idCharts: string;
  @Input() typeChart: string;
  isOnMobileMode : boolean = false;
  private chartP: am4charts.PieChart;
  private chart: am4charts.XYChart;
  tempData: any;
  dates = [];
  newData = [];
  gr = [];
  data = [];
  
  constructor(private _zone: NgZone, private _http: HttpClient) {
    
    }

  ngAfterViewInit() {
    console.log(this.idCharts)
    if (this.typeChart === 'Pie') {
      this.getPieChart();
    } else if(this.typeChart === 'Bar'){
      this.getBarChart();
    } else{
      this.getPieChart();
    }
  }

  detectScreenSize(event?: any) {
    let size;
    if (!event) {
      size = window.innerWidth;
    } else {
      size = event.target.innerWidth;
    }
    this.isOnMobileMode = size <= 871;// 871 is our mobile breakpoint
  }

  @HostListener("window:resize", ['$event'])
  onResize(event) {
    this.detectScreenSize(event);
  }

  getBarChart(){
    this._zone.runOutsideAngular(() => {
      let chart = am4core.create(this.idCharts, am4charts.XYChart);
      chart.data = [{
        "country": "USA",
        "visits": 2025
       }, {
        "country": "China",
        "visits": 1882
       }, {
        "country": "Japan",
        "visits": 1809
       }, {
        "country": "Germany",
        "visits": 1322
       }, {
        "country": "UK",
        "visits": 1122
       }, {
        "country": "France",
        "visits": 1114
       }, {
        "country": "India",
        "visits": 984
       }, {
        "country": "Spain",
        "visits": 711
       }, {
        "country": "Netherlands",
        "visits": 665
       }, {
        "country": "Russia",
        "visits": 580
       }, {
        "country": "South Korea",
        "visits": 443
       }, {
        "country": "Canada",
        "visits": 441
       }];
       
       chart.padding(40, 40, 40, 40);
       
       var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
       categoryAxis.renderer.grid.template.location = 0;
       categoryAxis.dataFields.category = "country";
       categoryAxis.renderer.minGridDistance = 60;
       categoryAxis.renderer.inversed = true;
       categoryAxis.renderer.grid.template.disabled = true;
       
       var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
       valueAxis.min = 0;
       valueAxis.extraMax = 0.1;
       //valueAxis.rangeChangeEasing = am4core.ease.linear;
       //valueAxis.rangeChangeDuration = 1500;
       
       var series = chart.series.push(new am4charts.ColumnSeries());
       series.dataFields.categoryX = "country";
       series.dataFields.valueY = "visits";
       series.tooltipText = "{valueY.value}"
       series.columns.template.strokeOpacity = 0;
       series.columns.template.column.cornerRadiusTopRight = 10;
       series.columns.template.column.cornerRadiusTopLeft = 10;
       //series.interpolationDuration = 1500;
       //series.interpolationEasing = am4core.ease.linear;
       var labelBullet = series.bullets.push(new am4charts.LabelBullet());
       labelBullet.label.verticalCenter = "bottom";
       labelBullet.label.dy = -10;
       labelBullet.label.text = "{values.valueY.workingValue.formatNumber('#.')}";
       
       chart.zoomOutButton.disabled = true;
       
       // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
       series.columns.template.adapter.add("fill", function (fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
       });
       
       setInterval(function () {
        am4core.array.each(chart.data, function (item) {
          item.visits += Math.round(Math.random() * 200 - 100);
          item.visits = Math.abs(item.visits);
        })
        chart.invalidateRawData();
       }, 2000)
       
       categoryAxis.sortBySeries = series;
      this.chart = chart;
    });
  }
  getPieChart(){
    this._zone.runOutsideAngular(() => {
      let chartP = am4core.create(this.idCharts, am4charts.PieChart);
      chartP.data = [{
        "country": "Lithuania",
        "litres": 520
      }, {
        "country": "Czech Republic",
        "litres": 480
      }];
      
      // Add and configure Series
      var pieSeries = chartP.series.push(new am4charts.PieSeries());
      pieSeries.dataFields.value = "litres";
      pieSeries.dataFields.category = "country";

      // Let's cut a hole in our Pie chart the size of 40% the radius
      chartP.innerRadius = am4core.percent(40);

      // Disable ticks and labels
      pieSeries.labels.template.disabled = true;
      pieSeries.ticks.template.disabled = true;

      // Disable tooltips
      pieSeries.slices.template.tooltipText = "";

      // Add a legend
      //chartP.legend = new am4charts.Legend();
      // Add label
      chartP.innerRadius = 50;
      let label = chartP.seriesContainer.createChild(am4core.Label);
      label.text = "48%";
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 20;
      this.chartP = chartP;
    });
  }

  ngOnDestroy() {
    this._zone.runOutsideAngular(() => {if (this.chart) this.chart.dispose();});
  }
}