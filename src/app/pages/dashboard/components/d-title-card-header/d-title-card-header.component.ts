import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rbit-d-title-card-header',
  templateUrl: './d-title-card-header.component.html',
  styleUrls: ['./d-title-card-header.component.scss']
})
export class DTitleCardHeaderComponent implements OnInit {
  @Input() cardTitle: string;
  @Input() cardTitleButton: boolean = false;
  @Input() cardGridButton: boolean = false;
  @Input() cardSubTitle: string;
  constructor() { }

  ngOnInit() {
  }

}
