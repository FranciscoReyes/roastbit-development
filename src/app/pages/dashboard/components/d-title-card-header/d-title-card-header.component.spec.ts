import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DTitleCardHeaderComponent } from './d-title-card-header.component';

describe('DTitleCardHeaderComponent', () => {
  let component: DTitleCardHeaderComponent;
  let fixture: ComponentFixture<DTitleCardHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DTitleCardHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DTitleCardHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
