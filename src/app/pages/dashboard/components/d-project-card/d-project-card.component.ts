import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'd-project-card',
  templateUrl: 'd-project-card.component.html',
  styleUrls: ['d-project-card.component.scss']
})
export class DProjectCardComponent implements OnInit, OnChanges {

  @Input() projectName: string;
  @Input() projectVersion: string;
  @Input() projectGoal: string;
  @Input() imgSource: string;
  @Input() projectTestCount: number = 0;
  @Input() projectPercentage: number = 0;
  @Input() projectSuccessPercentage: number = 0;
  @Input() projectErrorPercentage: number = 0;

  displayProgressBar: boolean = true;

  constructor() {
    this.displayProgressBar = (this.projectSuccessPercentage
      + this.projectErrorPercentage) <= 100;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.toggleProgressBar(changes);
  }

  toggleProgressBar(changes: SimpleChanges) {
    const success = !!changes['projectSuccessPercentage']
      ? changes['projectSuccessPercentage'].currentValue : 0;
    const error = !!changes['projectErrorPercentage']
      ? changes['projectErrorPercentage'].currentValue : 0;
    this.displayProgressBar = (success + error) <= 100;
  }
}
