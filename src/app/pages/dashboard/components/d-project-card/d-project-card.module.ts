import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {DProjectCardComponent} from './d-project-card.component'
import { ThemeModule } from '../../../../theme/theme.module';

@NgModule({
    imports: [CommonModule, FormsModule, ThemeModule],
    exports: [DProjectCardComponent],
    declarations: [DProjectCardComponent]
})
export class DProjectCardModule {}