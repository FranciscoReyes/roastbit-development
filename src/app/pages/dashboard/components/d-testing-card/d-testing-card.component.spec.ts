import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DTestingCardComponent } from './d-testing-card.component';

describe('DTestingCardComponent', () => {
  let component: DTestingCardComponent;
  let fixture: ComponentFixture<DTestingCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DTestingCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DTestingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
