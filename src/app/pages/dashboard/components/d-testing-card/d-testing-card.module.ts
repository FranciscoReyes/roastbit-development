import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DTestingCardComponent } from './d-testing-card.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    exports: [DTestingCardComponent],
    declarations: [DTestingCardComponent]
})
export class DTestingCardModule {}