import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'd-testing-card',
  templateUrl: 'd-testing-card.component.html',
  styleUrls: ['d-testing-card.component.scss']
})
export class DTestingCardComponent implements OnInit {

  @Input() testingName          : string;
  @Input() tag                  : any;
  @Input() status                  : any;
  @Input() projectName          : string;
  @Input() isTestingSuccess     : boolean;
  @Input() testingDescription   : string;
  @Input() testingAuthor        : string;
  @Input() testingDate          : string;// yyyy-MM-dd'T'hh:mm:ss
  @Input() imgTesting           : string;
  constructor() { }

  ngOnInit() {
  }
  getClassByTag(data: any = "") {
    let status;
    switch (data.toLowerCase()) {
      case 'nueva': {
        status = 'd-status';
      }break;
      case 'pendiente': {
        status = 'rejected';
      }break;
      case 'aprobada': {
        status = 'success';
      }break;
      case 'cancelada': {
        status = 'error';
      }break;
      case 'en proceso': {
        status = 'warning';
      }break;
    }
    return status;
  }

  getIconByTag(item: any = "") : string {
    switch (item) {
      case 'success': {
        return 'e917';
      }
      case 'error': {
        return 'e919';
      }
      case 'warning': {
        return 'e918';
      }
      default: {
        return 'e91a';
      }
    }
  }
}
