import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DTitleCardHeaderComponent } from './d-title-card-header/d-title-card-header.component';
import { DCardTestComponent } from './d-card-test/d-card-test.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    exports: [
        DCardTestComponent,
        DTitleCardHeaderComponent
    ],
    declarations: [
        DCardTestComponent,
        DTitleCardHeaderComponent
    ]
})
export class CommonComponentsModule {}