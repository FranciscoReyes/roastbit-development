import { Component } from '@angular/core';

@Component({
  selector: 'pages',
  template: `
    <rbit-header></rbit-header>
    <rbit-sidebar (onOpenSidebar)="menuCollapse = !menuCollapse"></rbit-sidebar>
    <div [ngClass]="menuCollapse ? 'main-page-layout collapse' : 'main-page-layout' ">
      <div class="main-container">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styleUrls: ["./pages.component.scss"]
})

export class PagesComponent {
  menuCollapse = false
}
