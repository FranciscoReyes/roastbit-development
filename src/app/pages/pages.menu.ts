export const PAGES_MENU= [
  {
    title: "Dashboard",
    icon: "e929",
    route: "/pages/dashboard/client"
  },
  {
    title: "Proyectos",
    icon: "e936",
    route: "/pages/projects"
  },
  // {
  //   title: "Pruebas",
  //   icon: "",
  //   route: "/pages/testing"
  // }
];
