import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';


const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./access/pages/login/login.module').then(m => m.LoginModule),
    pathMatch: "full"
  }, {
    path: 'access',
    loadChildren: () => import('./access/access.module').then(m => m.AccessModule)
  }, {
    path: 'pages',
    component: PagesComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },{
        path: 'projects',
        loadChildren: () => import('./project/project.module').then(m => m.ProjectModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
