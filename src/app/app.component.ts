import {AfterViewInit, Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'Roastbit';

  ngAfterViewInit(){

    document.querySelector("body").classList.add("theme-light");
   /*
    setTimeout(()=>{
       document.querySelector("body").classList.add("theme-dark");
    },5000)
    */


  }
}
