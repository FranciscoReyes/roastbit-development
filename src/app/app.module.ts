import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ApiCrudService} from "./shared/services/ApiCrud.service";
import {StorageService} from "./shared/services/storage.service";
import { PagesModule } from './pages/pages.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule
  ],
  providers: [ApiCrudService, StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
