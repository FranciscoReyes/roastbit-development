import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicInputComponent } from './components/basic-input/basic-input.component';
import { BasicButtonComponent } from './components/basic-button/basic-button.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import {RbitSimpleInputComponent} from "./components/simple-input/simple-input.component";
import {FormsModule} from "@angular/forms";
import { IconButtonComponent } from './components/icon-button/icon-button.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import {RoastbitTableComponent} from "./components/roastbit-table/roastbit-table.component";
import {SafeHtmlPipe} from "./pipes/safeHtml.pipe";
import {ModalChangeVersionComponent} from "../pages/project/components/modal-change-version/modal-change-version.component";

const COMPONENTS_THEME = [
  BasicInputComponent,
  BasicButtonComponent,
  HeaderComponent,
  SidebarComponent,
  RbitSimpleInputComponent,
  IconButtonComponent,
  RoastbitTableComponent,
  ProgressBarComponent,
  SafeHtmlPipe,
  ModalChangeVersionComponent
];

@NgModule({
  declarations: [
    ...COMPONENTS_THEME,
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
    exports: [
        ...COMPONENTS_THEME,
    ]
})
export class ThemeModule { }
