import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rbit-basic-button',
  templateUrl: './basic-button.component.html',
  styleUrls: ['./basic-button.component.scss']
})
export class BasicButtonComponent implements OnInit {

  @Input() isOutline: boolean = false;
  @Input() expand: 'block' | 'full' = 'block';
  @Input() bgColor : 'yellow' | 'red' | 'red-2' = 'red';
  @Input() withOutBackground: boolean = false;

  constructor() { }

  ngOnInit() {}

  getTextWithOutBackground(){
    return this.bgColor ==='yellow'  ? 'withoutBackground-yellow': 'withoutBackground-red';
  }
}
