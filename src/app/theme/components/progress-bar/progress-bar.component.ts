import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'rbit-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

  @Input() showPercentLabel           : boolean = false;
  @Input() paddingTop                 : number;
  @Input() paddingBottom                 : number;
  @Input() projectSuccessPercentage   : number  =  0;
  @Input() projectWarningPercentage   : number  =  0;
  @Input() projectInfoPercentage      : number  =  0;
  @Input() projectErrorPercentage     : number  =  0;

  constructor() { }

  ngOnInit() {
  }

}
