import {Component, OnInit, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import {Subject} from "rxjs";
import {RbitUtilsService} from "../../services/utils/utils.service";

let nextUniqueId = 0;
let nextUniqueIdLabel = 0;

@Component({
  selector: 'rbit-simple-input',
  templateUrl: 'simple-input.component.html',
  styleUrls: ['simple-input.component.scss']
})
export class RbitSimpleInputComponent implements OnInit, OnDestroy {

  @Input() anId: string | number;
  @Input() aPlaceholder : string;
  @Input() aValue : string;
  @Input() colorOnFocus: 'yellow' | 'red' = 'yellow';

  /**
   * For random id
   * @type {string}
   * @private
   */
  _uid = `rbit-input-${nextUniqueId++}`;

  /**
   * For random id for label
   * @type {string}
   * @private
   */
  _uidLabel = `rbit-form-field-label-${nextUniqueIdLabel}`;

  /**
   * For random id
   * @type {string}
   */
  controlType : string = 'rbit-input';

  /**
   * Detects new changes on this component
   * @type {Subject<void>}
   */
  readonly stateChanges : Subject<void> = new Subject<void>();

  /**
   * Detects when input is focused or blur
   * @type {boolean}
   */
  focused: boolean = false;

  @Input()
  get readonly(): boolean {return this._readonly}
  set readonly(value: boolean) {this._readonly = this._utils.coerceBooleanProperty(value);}
  private _readonly = false;

  @Output()
  @Output () value: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _utils: RbitUtilsService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.stateChanges.complete();
  }

  _focusChanged(isFocused: boolean) {
    if (isFocused !== this.focused && (!this.readonly || !isFocused)) {
      this.focused = isFocused;
      this.stateChanges.next();
    }
  }

  _onInput() {
    // This is a noop function and is used to let Angular know whenever the value changes.
    // Angular will run a new change detection each time the `input` event has been dispatched.
    // It's necessary that Angular recognizes the value change, because when floatingLabel
    // is set to false and Angular forms aren't used, the placeholder won't recognize the
    // value changes and will not disappear.
    // Listening to the input event wouldn't be necessary when the input is using the
    // FormsModule or ReactiveFormsModule, because Angular forms also listens to input events.
  }

  _handleChange(event) {
    this.value.emit(event.target.value)
  }
}
