import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoastbitTableComponent } from './roastbit-table.component';

describe('BWLTableComponent', () => {
  let component: RoastbitTableComponent;
  let fixture: ComponentFixture<RoastbitTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoastbitTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoastbitTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
