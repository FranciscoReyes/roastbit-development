import {
  Component,
  OnInit,
  ComponentFactoryResolver,
  ViewContainerRef,
  ViewChild,
  Input,
  Output,
  ViewEncapsulation, EventEmitter
}
  from "@angular/core";
import * as _ from 'lodash';

@Component({
  selector: 'roastbit-table-component',
  templateUrl: 'roastbit-table.component.html',
  styleUrls: ['roastbit-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RoastbitTableComponent implements OnInit {

  @ViewChild('componentFactory', { static: false, read: ViewContainerRef })
  private componentFactory                   : ViewContainerRef;

  @Input() dataSource                 : any               =   [];
  @Input() columnDef                  : any               =   [];
  @Input() fullCellRenderedComponent  : any               =   null;
  @Output() rowClicked                : EventEmitter<any> =   new EventEmitter<any>();
  @Output() cellClicked               : EventEmitter<any> =   new EventEmitter<any>();

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }

  ngOnInit() { }

  showTableDetail(item, itemIndex){
    this.dataSource.forEach((d, index)=> {
      if (itemIndex == index){
        d.expanded = !d.expanded;
      } else {
        d.expanded = false;
      }
    });
    this.getComponent(item)
  }

  getComponent(data){
    setTimeout(()=> {
      if(!this.componentFactory){
        return null;
      }else {
        if (this.fullCellRenderedComponent) {
          let expComponent: any = this.componentFactory.createComponent(this.componentFactoryResolver.resolveComponentFactory(this.fullCellRenderedComponent));
          if (expComponent.instance) {
            expComponent.instance.dataFromParent = data;
            expComponent.instance.fullDataFromParent = this.dataSource;
          }
        }
      }
    });
  }

  onCellClicked(item, element) {
    let eventEmit = _.cloneDeep({data:item, colDef:element});
    if(eventEmit.colDef.cellRenderer) delete eventEmit.colDef['cellRenderer'];
    this.cellClicked.emit(eventEmit);
  }
}
