import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'rbit-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  activateSearchIcon: boolean = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  toggleSearch(activate) {
    this.activateSearchIcon = activate;
  }

  openNewProjectForm() {
    this.router.navigateByUrl('/pages/projects/new');
  }
}
