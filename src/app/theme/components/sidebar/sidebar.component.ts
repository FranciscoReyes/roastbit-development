import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Router} from "@angular/router";
import {PAGES_MENU} from "../../../pages/pages.menu";

@Component({
  selector: 'rbit-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Output() onOpenSidebar = new EventEmitter();
  @Output() onCloseSession = new EventEmitter();

  expand = false;

  themeDark = false;
  menuCollection = [];


  constructor(private router: Router) {

    this.menuCollection = PAGES_MENU;

    this.router.events.subscribe((event:any)=> {
      if (event.url) {
        this.clearSelection();
        let item = this.menuCollection.find(d=>d.route == event.url);
        if (item)
          item.active = true;
      }
    });


  }

  // collapseSidebar() {
    // this.expand = !this.expand
    // this.onOpenSidebar.emit();
  // }

  closeSession() {
    this.onCloseSession.emit();
  }


  performSelection(item){
    this.clearSelection();
    item.active = true;
    this.router.navigateByUrl(item.route);
  }
  clearSelection() {
    this.menuCollection.forEach(d=>d.active = false);
  }

  changeTheme() {
    if (this.themeDark) {
      document.querySelector("body").classList.add("theme-light");
      document.querySelector("body").classList.remove("theme-dark");
    } else {
      document.querySelector("body").classList.add("theme-dark");
      document.querySelector("body").classList.remove("theme-light");
    }
    this.themeDark = !this.themeDark;
  }


  ngOnInit() {
  }

}
