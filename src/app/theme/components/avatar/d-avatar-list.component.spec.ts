import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DAvatarListComponent } from './d-avatar-list.component';

describe('DAvatarListComponent', () => {
  let component: DAvatarListComponent;
  let fixture: ComponentFixture<DAvatarListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DAvatarListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DAvatarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
