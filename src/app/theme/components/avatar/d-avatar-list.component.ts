import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rbit-d-avatar-list',
  templateUrl: './d-avatar-list.component.html',
  styleUrls: ['./d-avatar-list.component.scss']
})
export class DAvatarListComponent implements OnInit {
  @Input()projectAvatar: any =[];
  constructor() { }

  ngOnInit() {
  }

}
