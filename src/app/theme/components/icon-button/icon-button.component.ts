import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rbit-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss']
})
export class IconButtonComponent implements OnInit {

  /**
   * Set an icon code to show in button
   * 
   * If you set this input you CAN NOT set 'text' input
   */
  @Input() iconCode: string;

  /**
   * Set a text is you only want to show a simple text
   * 
   * If you set this input you CAN NOT set 'iconCode' input
   */
  @Input() text: string;

  constructor() { }

  ngOnInit() {
  }

}
