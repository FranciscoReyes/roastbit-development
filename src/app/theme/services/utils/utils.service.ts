import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RbitUtilsService {

  constructor() { }

  /**
   * Coerces a data-bound value (typically a string) to a boolean
   * @param value
   * @returns {boolean}
   */
  coerceBooleanProperty(value : any): boolean {
    return value != null && `${value}` !== 'false';
  }
}
